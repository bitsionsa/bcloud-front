import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';

import { NgZorroAntdModule} from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HomeModule } from 'src/app/home/home.module';


@NgModule({
  declarations: [NavbarComponent, FooterComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule, 
    FormsModule,HomeModule
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
  ]
})
export class SharedModule { }
