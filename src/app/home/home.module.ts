import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { BannerComponent } from './components/banner/banner.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { BenefitsComponent } from './components/benefits/benefits.component';
import { FeaturesComponent } from './components/features/features.component';
import { InfoHelpComponent } from './components/info-help/info-help.component';


@NgModule({
  declarations: [HomeComponent, BannerComponent, AboutUsComponent, BenefitsComponent, FeaturesComponent, InfoHelpComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    RouterModule,
    NgZorroAntdModule,
    FormsModule
  ]
})
export class HomeModule { }
