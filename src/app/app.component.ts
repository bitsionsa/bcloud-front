import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(@Inject(DOCUMENT) private document: Document) { }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (document.body.scrollTop > 60 ||
      document.documentElement.scrollTop > 60) {
      document.getElementById('navbar').classList.add('position-fixed');
      document.getElementById('navbar').classList.remove('bg-transparent');
      document.getElementById('navbar').classList.add('bg-bc-black-50');
    }
    else {
      document.getElementById('navbar').classList.add('bg-transparent');
      document.getElementById('navbar').classList.remove('bg-bc-black-50');
    }
  }
  title = 'appName';
}
