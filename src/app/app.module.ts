import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, es_ES, NzIconModule } from 'ng-zorro-antd';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import localeEsAr from '@angular/common/locales/es-AR';
import localeEs from '@angular/common/locales/es';
registerLocaleData(localeEs, 'es');
registerLocaleData(localeEsAr, 'es-Ar');

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    NgZorroAntdModule,
    HttpClientModule,
    ReactiveFormsModule,
    NzIconModule,
    SharedModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: es_ES },
    { provide: LOCALE_ID, useValue: "es-Ar" },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
